import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_6_Test_Niepoprawnego_Logowania_Password_Test {
    @Test
    public void incorrectPasword(){

            System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get("http://localhost:4444");

            WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
            emailTxt.sendKeys("test@test.com");

            WebElement passwordTxt = driver.findElement(By.cssSelector(("#Password")));
            passwordTxt.sendKeys("jskasjkjas");

            WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
            loginBtn.click();

            List<WebElement> validationError = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));

            boolean doesErrorExists = false;
            for(int i=0; i<validationError.size();i++){
                if(validationError.get(i).getText().equals("Invalid login attempt.")){
                    doesErrorExists = true;
                    break;
                }
            }
            Assert.assertTrue(doesErrorExists);
        }




    }


