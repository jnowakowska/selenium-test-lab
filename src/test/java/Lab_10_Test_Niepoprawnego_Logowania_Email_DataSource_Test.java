import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumBaseTest {
    protected WebDriver driver;

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"},
        };

    }

    @Test(dataProvider = "getWrongEmails")
    public void IncorrectEmailTest(String wrongEmail) {
        ;

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail(wrongEmail);
        loginPage.typePassword("Test1!");
        loginPage.submitLoginWithFailure();
        loginPage.assertEmailError("The email field is not a valid e-mail address.");
    }

    @Test(dataProvider = "getWrongEmails")
    public void IncorrectEmailTestWithChaining(String wrongEmail) {
        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .typePassword("Test1!")
                .submitLoginWithFailure()
                .assertEmailError("The email field is not a valid e-mail address.");
    }
}


        //Assert.assertEquals(loginPage.emailError.getText(), ("The Email field is not a valid e-mail address."));







