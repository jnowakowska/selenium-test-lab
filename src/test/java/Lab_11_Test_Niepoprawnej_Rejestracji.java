import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_11_Test_Niepoprawnej_Rejestracji {
    protected WebDriver driver;

    @DataProvider
    public Object[] [] getWrongEmails1() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"},
        };
    }

    @Test(dataProvider = "getWrongEmails1")
    public void IncorrectEmailRegistration(String wrongEmails) {


        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");

        WebElement registerBtn = driver.findElement(By.cssSelector(".change_link>a"));

        registerBtn.click();

        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.typeEmail(wrongEmails);
        createAccountPage.typePassword("blabla");
        createAccountPage.typeConfirmation("blabla");
        createAccountPage.submitLoginWithFailure();

        Assert.assertEquals(createAccountPage.emailError.getText(), ("The Email field is not a valid e-mail address."));


        driver.quit();
    }
    }

