import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_9_Test_Niepoprawnego_Logowania extends SeleniumBaseTest {


    @Test
    public void incorrectLoginTest() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test1234@test.com");
        loginPage.typePassword("Test1!");
        loginPage.submitLoginWithFailure();
        loginPage.assertDoesErrorExist("Invalid login attempt.");
    }

    @Test
    public void incorrectLoginTestWithChaining() {
        new LoginPage(driver)
                .typeEmail("test1234@test.com")
                .typePassword("Test1!")
                .submitLoginWithFailure()
                .assertDoesErrorExist("Invalid login attempt.");
    }
}








