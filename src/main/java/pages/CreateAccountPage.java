package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "Confirm Password")
    private WebElement confirmPasswordTxt;

    @FindBy(css="button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css="#Email-error")
    public WebElement emailError;

    public CreateAccountPage typeEmail(String email){
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password){
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmation(String password){
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }


    public HomePage submitLogin(){
        loginBtn.click();

        return new HomePage(driver);
    }
    public CreateAccountPage submitLoginWithFailure(){
        loginBtn.click();

        return this;

}
    }