package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateProcessPage extends ProcessesPage {
    public CreateProcessPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "input[type = submit")
    private WebElement createBtn;

    @FindBy(css = "#Name")
    private WebElement processName;

    @FindBy(css = ".field-validation-error[data-valmsg-for=Name]")
    public WebElement nameEror;

    @FindBy(css = "a.btn[href*=Projects]")
    public WebElement backToListBtn;


    public CreateProcessPage typeName(String process) {
        processName.clear();
        processName.sendKeys(process);
        return this;
    }

    public CreateProcessPage submitCreate() {
        createBtn.click();

        return new CreateProcessPage(driver);
    }

    public CreateProcessPage submitCreateWithFailure() {
        createBtn.click();

        return this;
    }


    public CreateProcessPage assertProcessNameError(String expErrorMessage) {
        Assert.assertTrue(nameEror.getText().contains(expErrorMessage));
        return this;
    }



    public CreateProcessPage backToList() {
        backToListBtn.click();

        return new CreateProcessPage(driver);

    }

}

