package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {
    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects")
    private WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics")
    private WebElement characteristicMenu;



    public HomePage assertWelcomeElementIsShown() {
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"));
        return this;
    }


    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses(){
        if(!isParentExpanded(workspaceNav))
        workspaceNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));
        processesMenu.click();
        return new ProcessesPage(driver);
    }

    public CharacteristicsPage goToCharacteristics(){
        if(!isParentExpanded(workspaceNav))
            workspaceNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicMenu));
        characteristicMenu.click();
        return new CharacteristicsPage(driver);
    }

    public DashboardPage goToDashboard(){
        if(!isParentExpanded(workspaceNav))
            workspaceNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));
        processesMenu.click();
        return new DashboardPage(driver);
    }

}
