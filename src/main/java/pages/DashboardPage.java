package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage extends HomePage {
    public DashboardPage(WebDriver driver){
            super(driver);
            PageFactory.initElements(driver, this);
        }
    }

