package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(linkText = "Add new characteristic")
    private WebElement addNewCharacteristicsBtn;


    public CharacteristicsPage clickCharacteristic(){
        addNewCharacteristicsBtn.click();
        return new CharacteristicsPage(driver);


    }
}

